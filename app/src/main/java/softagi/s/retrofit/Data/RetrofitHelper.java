package softagi.s.retrofit.Data;

import java.util.List;

import retrofit2.Call;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import softagi.s.retrofit.Models.NewsModel;
import softagi.s.retrofit.Models.Post;

public interface RetrofitHelper
{
    @GET("posts")
    Call<List<Post>> getPosts();

    @POST("posts")
    Call<Post> setPost1(@Body Post post);

    @POST("posts")
    @FormUrlEncoded
    Call<Post> setPost2(
            @Field("userId") int userId,
            @Field("title") String title,
            @Field("body") String body
    );

    @PUT("posts/{id}")
    Call<Post> updatePost1(
            @Path("id") int id,
            @Body Post post
    );

    @PATCH("posts/{id}")
    Call<Post> updatePost2(
            @Path("id") int id,
            @Field("userId") int userId,
            @Field("title") String title,
            @Field("body") String body
    );

    @DELETE("posts/{id}")
    Call<Post> deletePost(@Path("id") int id);
}