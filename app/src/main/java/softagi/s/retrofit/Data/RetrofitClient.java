package softagi.s.retrofit.Data;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import softagi.s.retrofit.Models.NewsModel;
import softagi.s.retrofit.Models.Post;

public class RetrofitClient
{
    private final static String BASE_URL = "http://newsapi.org/";
    private static RetrofitClient retrofitClient;
    private RetrofitHelper retrofitHelper;

    private RetrofitClient()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitHelper = retrofit.create(RetrofitHelper.class);
    }

    public static RetrofitClient getInstance()
    {
        if (retrofitClient == null)
        {
            retrofitClient = new RetrofitClient();
        }

        return retrofitClient;
    }

    public Call<List<Post>> getPosts()
    {
        return retrofitHelper.getPosts();
    }

    public Call<Post> setPost1(Post post)
    {
        return  retrofitHelper.setPost1(post);
    }

    public Call<Post> setPost2(int userId, String title, String body)
    {
        return  retrofitHelper.setPost2(userId, title, body);
    }

    public Call<Post> updatePost1(int postId,Post post)
    {
        return  retrofitHelper.updatePost1(postId,post);
    }

    public Call<Post> updatePost2(int postId, int userId, String title, String body)
    {
        return  retrofitHelper.updatePost2(postId, userId, title, body);
    }

    public Call<Post> deletePost(int postId)
    {
        return retrofitHelper.deletePost(postId);
    }
}