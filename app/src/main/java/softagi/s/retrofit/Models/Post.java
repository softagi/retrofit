package softagi.s.retrofit.Models;

import com.google.gson.annotations.SerializedName;

public class Post
{
    @SerializedName("userId")
    private int userId;
    @SerializedName("id")
    private int postId;
    @SerializedName("title")
    private String postTitle;
    @SerializedName("body")
    private String postBody;

    public Post(int userId, int postId, String postTitle, String postBody) {
        this.userId = userId;
        this.postId = postId;
        this.postTitle = postTitle;
        this.postBody = postBody;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }
}
